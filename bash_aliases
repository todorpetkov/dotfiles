# Useful aliases
alias ll='ls -alF'

# Functions
parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}

genpasswd ()
{
    tr -dc '[:graph:]' < /dev/urandom | head -c ${1:-30} | xargs -0
}
