export VISUAL=/usr/bin/vim
export HISTSIZE=1000000
export HISTFILESIZE=1000000
export HISTTIMEFORMAT="[%F %T] "
export PS1='\[\e[0;38;5;166m\]\u\[\e[0m\]@\[\e[0;38;5;154m\]\H\[\e[0m\]:\[\e[0;38;5;38m\]\w \[\e[0m\]\[\e[0;38;5;71m\]($(parse_git_branch))\[\e[0m\]\$ '
